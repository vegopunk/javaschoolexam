package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if (statement == null) {
            return null;
        }

        if (statement.contains(",") || statement.isEmpty()) {
            return null;
        }

        ArrayList<String> operations = new ArrayList<>();
        operations.add("+");
        operations.add("*");
        operations.add("/");
        operations.add("-");
        operations.add(".");

        for (int i = 1; i < statement.length(); i++) {
            if (operations.contains(String.valueOf(statement.charAt(i-1))) && operations.contains(String.valueOf(statement.charAt(i)))) {
                return null;
            }
        }

        int rightBrace = 0;
        int leftBrace = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') leftBrace++;
            if (statement.charAt(i) == ')') rightBrace++;
        }

        if (rightBrace != leftBrace) {
            return null;
        }


        String[] infixSource = parseToInfix(statement);
        String res = eval(convertInfixToRPN(infixSource));

        return res;
    }





    boolean isNumber(String str) {
        try{
            Double.valueOf(str);
            return true;
        } catch(Exception e){
            return false;
        }
    }

    Queue<String> convertInfixToRPN(String[] infixNotation) {
        Map<String, Integer> prededence = new HashMap<>();
        prededence.put("/", 5);
        prededence.put("*", 5);
        prededence.put("+", 4);
        prededence.put("-", 4);
        prededence.put("(", 0);

        Queue<String> Q = new LinkedList<>();
        Stack<String> S = new Stack<>();

        for (String token : infixNotation) {
            if ("(".equals(token)) {
                S.push(token);
                continue;
            }

            if (")".equals(token)) {
                while (!"(".equals(S.peek())) {
                    Q.add(S.pop());
                }
                S.pop();
                continue;
            }
            // an operator
            if (prededence.containsKey(token)) {
                while (!S.empty() && prededence.get(token) <= prededence.get(S.peek())) {
                    Q.add(S.pop());
                }
                S.push(token);
                continue;
            }

            if (isNumber(token)) {
                Q.add(token);
                continue;
            }
//
//            if (",".equals(token)) {
//                continue;
//            }

            throw new IllegalArgumentException("Invalid input");
        }
        // at the end, pop all the elements in S to Q
        while (!S.isEmpty()) {
            Q.add(S.pop());
        }

        return Q;
    }

    public Stack<String> stack = new Stack<>();

    public String eval(Queue<String> rpn) {
        while (!rpn.isEmpty()) {
            String el = rpn.poll();

            if (!(el.equals("+") ||
                    el.equals("-") ||
                    el.equals("/") ||
                    el.equals("*"))) {
                stack.push(el);
            } else {
                //calculate
                String operation = el;
                String first = stack.pop();
                String second = stack.pop();
                float firstF = Float.parseFloat(first);
                float secondF = Float.parseFloat(second);

                float result = 0.0f;

                if (operation.equals("+")) {
                    result = firstF + secondF;
                } else if (operation.equals("-")) {
                    result = secondF - firstF;
                } else if (operation.equals("*")) {
                    result = firstF * secondF;
                } else if (operation.equals("/")) {
                    result = secondF / firstF;
                }
                stack.push(String.valueOf(result));
            }
        }Double res = Double.parseDouble(stack.pop());
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator('.');

        DecimalFormat df = new DecimalFormat("#.#####", otherSymbols);
        String ept = df.format(res);
        //String sdf = String.valueOf(res);
        if ( res % 1 == 0 ) {
            long result = res.longValue();
            return String.valueOf(result);
        }
        if (Double.isInfinite(res)) {
            return null;
        }
        return ept;
    }

    public String[] parseToInfix(String source) {


        source = source.replaceAll("\\s+","");
        String[] outs = source.split("(?<=[-+*/\\(\\)])|(?=[-+*/\\(\\)])");


        ArrayList<String> listToWork = new ArrayList<>(Arrays.asList(outs));

        for (int i = 0; i < listToWork.size(); i++) {
            if (listToWork.get(i).equals("-")) {
                if (i == 0) {
                    Double nextEl = Double.parseDouble(listToWork.get(i+1));
                    nextEl *= -1;
                    String newEl = String.valueOf(nextEl);
                    listToWork.remove(0);
                    listToWork.remove(0);
                    listToWork.add(0, newEl);

                }else if (listToWork.get(i-1).equals("(")) {
                    Double nextEl = Double.parseDouble(listToWork.get(i+1));
                    nextEl *= -1;
                    String newEl = String.valueOf(nextEl);
                    listToWork.remove(i);
                    listToWork.remove(i);
                    listToWork.add(i, newEl);
                }
            }
        }

        String[] res = listToWork.toArray(new String[0]);
        return res;
    }






}
