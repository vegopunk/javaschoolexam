package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.TreeSet;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers == null || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int listSize = inputNumbers.size();
        int rowsNumber = 0;

        while (listSize > 0) {
            listSize -= rowsNumber;
            if (listSize == 0) {
                break;
            } else if (listSize < 0) {
                throw new CannotBuildPyramidException();
            } else {
                rowsNumber++;
            }
        }

//        Collections.sort(inputNumbers);

        TreeSet sorted = new TreeSet();
        sorted.addAll(inputNumbers);

        int colsNumber = rowsNumber*2-1;
        int[][] result = new int[rowsNumber][colsNumber];

        for (int i = 0; i < rowsNumber; i++) {
            for (int j = 0; j < colsNumber; j++) {
                result[i][j] = 0;
            }
        }

        for (int i = 0; i<rowsNumber; i++) {
            int padding = rowsNumber-(i+1);
            for(int j = padding; j<colsNumber-padding; ) {
                result[i][j] = (int) sorted.pollFirst();
                j += 2;
            }
        }

        return result;
    }

}
